#include <stdio.h>

int valid_triangle (const float a, const float b, const float c)
{
    int megrajzolhato = 0;
    if (a > 0 && b > 0 && c > 0)
    {
        if(a + b > c && a + c > b && c + b > a)
        {
            megrajzolhato = 1;
        }
    }
    else
    {
        puts("Hiba! Pozitiv valos szamokat adjon meg!");
    }
    return megrajzolhato;
}

int main()
{
    float a,b,c = 0;

    puts("Adja meg az oldalak hosszat valos, pozitiv szammal!");
    printf("a: ");
    scanf("%f", &a);
    printf("b: ");
    scanf("%f", &b);
    printf("c: ");
    scanf("%f", &c);

    int eredmeny = valid_triangle (a,b,c);

    printf(eredmeny == 1 ? "Megrajzolhato" : "Nem rajzolhato meg");

    return 0;
}
