# FELADATOK

## Megrajzolható háromszögek - 20200228a

Írjunk egy függvényt, ami egy háromszögről eldönti, hogy megszerkeszthető-e vagy sem. 

## Páros? Páratlan? - 20200305e

Írjon egy függvényt, ami megkap egy egész számot, és ha az átadott szám páros, akkor 1-gyel (igazzal) tér vissza, különben 0-val (hamissal). Írjon egy függvényt, ami megkap egy egész számot, és ha az átadott szám páratlan, akkor 1-gyel tér vissza, különben 0-val. 

## Tömb rendezett-e? - 20200305a

Írjunk függvényt, mely egy adott tömbről eldönti, hogy rendezett-e. A tömb egész számokat tartalmaz. Az üres tömböt tekintsük rendezettnek. 

## Tömb legkisebb / legnagyobb eleme - 20200305b

Írjunk fv.-t, ami visszaadja egy egészeket tartalmazó tömb legkisebb elemét. Írjunk fv.-t, ami visszaadja egy egészeket tartalmazó tömb legnagyobb elemét.

## felhőkarcolók - 20200305c

Vegyük a 2^1024 számot. Ha ezen szám számjegyeit felhőkarcolók magasságának tekintjük, akkor mennyi lesz a szomszédos felhőkarcolók magasságkülönbségének az összege? 

## Tömb elemei, vesszővel elválasztva - 20200305f

Írjon eljárást, ami kap egy tömböt, s kiírja a tömb elemeit egymás mellé. Az elemek között legyen vessző és szóköz. Az utolsó elem után csak sortörés szerepeljen! 

## pozitív hozzáállás - 20200305h

Írjon eljárást, ami kap egy egészeket tartalmazó tömböt. Az eljárás cserélje ki a tömbben lévő negatív számokat az abszolútértékükre! Az eljárás a tömböt helyben módosítja. 

## Tömb elemeinek szorzata
## Tömb elemeinek átlaga
## Benne van-e a tömbben?
## Keresett elem indexe
## Üres-e a tömb?
