#include <stdio.h>

double avg (double hossz, int tomb[])
{
    int osszeg = 0;

    for(int i = 0; i < hossz; i++)
    {
        osszeg += tomb[i];
    }

    return osszeg / hossz;
}

int main()
{
    int szamok[] = { 1, 2, 4, 4};
    int tombnagysag = sizeof(szamok) / sizeof(szamok[0]);

    printf("A tomb elemeinek atlaga: %lf", avg(tombnagysag, szamok));

    return 0;
}
