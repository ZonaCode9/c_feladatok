#include <stdio.h>

int ordered (int length, int tomb[])
{
    int is_ordered = 1;

    for(int i = 0; i < length-1; i++)
    {
        if (tomb[i] > tomb[i+1])
        {
            is_ordered = 0;
            break;
        }
    }
    return is_ordered;
}

int main()
{
    int szamok[] = {1, 2, 2, 5, 10, 1};

    int hossza = sizeof(szamok) / sizeof(szamok[0]);

    int rendezette = ordered(hossza, szamok);

    printf(rendezette == 1 ? "Rendezett" : "Nem rendezett");

    return 0;
}
