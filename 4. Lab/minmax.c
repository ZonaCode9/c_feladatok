#include <stdio.h>
#include <limits.h>

int minimum (int length, int tomb[])
{
    int minimum = tomb[0];
    for(int i = 1; i < length; i++)
    {
        if(tomb[i] < minimum)
        {
            minimum = tomb[i];
        }
    }
    return minimum;
}

int maximum (int length, int tomb[])
{
    int maximum = tomb[0];
    for(int i = 1; i < length; i++)
    {
        if(tomb[i] > maximum)
        {
            maximum = tomb[i];
        }
    }
    return maximum;
}


int main()
{
    int szamok[] = { 1, 2, 10, -1, 5 };

    int tombhossz = sizeof(szamok) / sizeof(szamok[0]);

    printf("A legnagyobb elem: %d, a legkisebb: %d \n", maximum(tombhossz, szamok), minimum(tombhossz, szamok));

    return 0;
}
