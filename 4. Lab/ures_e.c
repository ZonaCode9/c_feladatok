#include <stdio.h>

void ures_e(int meret, int tomb[])
{
    puts(meret == 0 ? "Ures a tomb." : "A tomb nem ures.");
}

int main()
{
    int tomb[] = {1};

    int meret = sizeof(tomb) / sizeof(tomb[0]);

    ures_e(meret, tomb);

    return 0;
}
