#include <stdio.h>

int abs(int szam)
{
    return szam > 0 ? szam : -szam;
}

void pozitivan(int hossz, int tomb[])
{
    for(int i = 0; i < hossz; i++)
    {
        tomb[i] = abs(tomb[i]);
    }
}

void kiir(int hossz, int tomb[])
{
    for(int i = 0; i < hossz-1; i++)
    {
        printf("%d ", tomb[i]);
    }
    printf("%d\n", tomb[hossz-1]);
}

int main()
{
    int elemek[] = { 10, 9, -8, 7, -6, 5, 4, 3, -2, 1, 0 };
    int hosszusag = sizeof(elemek) / sizeof(elemek[0]);

    kiir(hosszusag, elemek);

    pozitivan(hosszusag, elemek);

    kiir(hosszusag, elemek);

    return 0;
}
