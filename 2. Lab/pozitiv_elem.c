#include <stdio.h>

int main()
{
    int szam = 0;
    int osszeg = 0;

    do {

        printf("Egesz szam: ");
        scanf("%d", &szam);

        if(szam > 0)
        {
            osszeg ++;
        }

    } while (szam != 0);

    printf("A pozitiv elemek szama: %d \n", osszeg);

    return 0;
}