# FELADATOK

## Egész számok összege 1-től 100-ig - 20200213g

Írjunk programot, ami kiszámolja az egész számok összegét 1-től 100-ig. 

## Ezernél kisebb pozitív egész számok (PE #1) - 20200213h

Állapítsuk meg azon 1000-nél kisebb számok összegét, melyek 3-nak vagy 5-nek a többszörösei. 

## Elemek összege - 20200219a

Írjon programot, ami 0 végjelig egész számokat olvas be. Írja ki a számok összegét (a végjel természetesen nem számít).

## Pozitív elemek száma - 20200213m

Írjon programot, ami 0 végjelig egész számokat olvas be. Írja ki, hány darab pozitív számot adott meg a felhasználó (a végjel természetesen nem számít). 

## Pozitív és negatív elemek száma - 20200213n

Írjon programot, ami 0 végjelig egész számokat olvas be. Írja ki, hány darab pozitív és hány darab negatív számot adott meg a felhasználó (a végjel természetesen nem számít). 

## Kisebb, nagyobb, egyenlő - 20200220c

Írjunk egy programot, amely beolvas két pozitív egész számot (n1 és n2), majd a program írja ki azt a relációs jelet, amit a két érték közé helyezhetünk (<, > vagy =)! 

## Néggyel osztható számok - 20200220a

Írjunk egy programot, amely beolvas egy pozitív egész számot (n), majd kiírja 1-től n-ig (zárt intervallum) az összes néggyel osztható számot! 

## Páratlan számok visszafelé - 20200220b

Írjunk egy programot, amely beolvas egy pozitív egész számot (n), majd kiírja n-től 1-ig (zárt intervallum) visszafelé az összes páratlan számot! 
