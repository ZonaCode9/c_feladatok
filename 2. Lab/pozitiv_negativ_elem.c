#include <stdio.h>

int main()
{
    int szam = 0;
    int pozitivak = 0;
    int negativak = 0;

    do {

        printf("Egesz szam: ");
        scanf("%d", &szam);

        if(szam > 0)
        {
            pozitivak ++;
        }

        if(szam < 0)
        {
            negativak ++;
        }

    } while (szam != 0);

    printf("A pozitiv elemek szama: %d \n", pozitivak);

    printf("A negativ elemek szama: %d \n", negativak);

    return 0;
}