#include <stdio.h>

int main()
{
    int a = 0;
    int b = 0;

    printf("Az elso szam: ");
    scanf("%d", &a);

    printf("A masodik szam: ");
    scanf("%d", &b);

    if(a > b)
    {
        printf("> \n");
    }
    else if(a < b)
    {
        printf("< \n");
    }
    else
    {
        printf("= \n");
    }

    return 0;
}