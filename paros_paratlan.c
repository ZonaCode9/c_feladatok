#include <stdio.h>

int is_even(const int n)
{
    return n % 2 == 0 ? 1 : 0;
}

int is_odd(const int n)
{
    return n % 2 != 0 ? 1 : 0;
}

int is_odd_rec(const int n)
{
    return is_even(n) != 1 ? 1 : 0;
}

void eldont(const int paros, const int paratlan)
{
    if(paros == 1 && paratlan == 1)
    {
        puts("HIBA!");
    }
    else if(paros == 1)
    {
        puts("A megadott szam paros.");
    }
    else
    {
        puts("A szam paratlan");
    }
}

int main()
{
    int szam = 0;
    printf("Adjon megy egy egesz szamot! \n");
    scanf("%d", &szam);

    int paros_e = is_even(szam);
    int paratlan_1 = is_odd(szam);
    int paratlan_2 = is_odd_rec(szam);

    eldont(paros_e, paratlan_1);

    return 0;
}
