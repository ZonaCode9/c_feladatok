#include <stdio.h>

int include(int nagysag, int keresett, int tomb[])
{
    int index = -1;
    for(int i = 0; i < nagysag; i++)
    {
        if(tomb[i] == keresett)
        {
            index = i;
            break;
        }
    }
    return index;
}

int main()
{
    int keresett;
    puts("Adja meg a keresett szmot!");
    scanf("%d", &keresett);

    int szamok[] = { 1, 2, 3, 5, 6, 7 };
    int tomb_nagysaga = sizeof(szamok) / sizeof(szamok[0]);

    int hely = include(tomb_nagysaga, keresett, szamok);

    if(hely != -1)
    {
       printf("A keresett szam indexe: %d \n", hely);
    }
    else
    {
        puts("A keresett elem nincs benne a tombben.");
    }

    return 0;
}
