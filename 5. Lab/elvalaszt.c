#include <stdio.h>

//ez a fuggveny egy megadott hosszusagu, megadott karakterekbol allo elvalasztot ir ki
void line(char c, int length)
{
    for( int i = 0; i < length; i++)
    {
        putchar(c);
    }
    puts("");
}

int main()
{
    printf("Hello \n");

    line('-', 40);

    printf("World! \n");

    return 0;
}