#include <stdio.h>
#include "prog1.h"

//megtalalja egy adott karakter utolso elofordulasat egy stringben
int rfind_char(string s, char c)
{
    int stringhossz = sizeof(s) - 1;

    for(int i = stringhossz; i > 0; i--)
    {
        if(s[i] == c)
        {
            return i;
        }
    }
    return -1;
}

int main()
{
    printf("%d \n", rfind_char("Abba", 'b'));

    return 0;
}