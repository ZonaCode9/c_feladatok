#include <stdio.h>
#include "prog1.h"

//megszamolja egy adott karakter elofordulasanak szamat egy stringben
int char_count(string s, char c)
{
    int stringhossz = sizeof(s) - 1;
    int elofordul = 0;

    for(int i = 0; i < stringhossz; i++)
    {
        if(s[i] == c)
        {
            elofordul++;
        }
    }
    return elofordul;
}

int main()
{
    printf("%d \n", char_count("Abba",'a'));

    return 0;
}