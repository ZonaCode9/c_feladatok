#include <stdio.h>
#include <string.h>
#include "prog1.h"

//megszamolja a stringeket, amelyek legalabb 2 karater hosszuak es az elso es utolso karateruk megegyezik
int match_ends(int n, string words[])
{
    int szamlal = 0;

    for(int i = 0; i < n; i++)  //tombben iteral
    {
        int betuk = 0;
        string szo = words[i];
        while(szo[betuk] != '\0')
        {
            betuk++;
        }
        if((betuk >= 2) && (szo[0]==szo[betuk-1]))
        {
            szamlal++;
        }
    }
    return szamlal;
}

int main()
{
    string szavak1[] = { "aba", "xyz", "aa", "x", "bbb" };
    int szavak1_meret = 5;

    printf("%d \n", match_ends(szavak1_meret, szavak1));

    for(int i = 0; i < szavak1_meret; i++)
    {
        puts(szavak1[i]);
    }

    return 0;
}