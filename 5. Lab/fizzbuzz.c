#include <stdio.h>

//1-tol 100-ig kiirja a szamokat
//Ha a szam 5-el es 3-al oszthato: fizzbuzz
//Ha a szam csak harommal oszthato: fizz
//Ha a szam csak ottel oszthato: buzz
//Egyebkent kiirja a szamot
void fizzbuzz()
{
    for(int i = 1; i <= 100; i++)
    {
        if( i % 5 == 0 && i % 3 == 0)
        {
            puts("fizzbuzz");
        }
        else if( i % 3 == 0)
        {
            puts("fizz");
        }
        else if( i % 5 == 0)
        {
            puts("buzz");
        }
        else
        {
            printf("%d \n", i);
        }
    }
}

int main()
{
    fizzbuzz();

    return 0;
}