#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 1000

int main()
{
    char* fname = "in.txt";

    FILE *fp = fopen(fname, "r");

    if(fp == NULL)
    {
        fprintf(stderr, "Hiba! A %s nevu fajlt nem sikerult megnyitni! \n", fname);
        exit(1);
    }
    printf("# %s sikeresen megnyitva \n", fname);

    char line[MAX];
    int wrote = 0;

    FILE *out = fopen("out.txt", "w");

    printf("# 0,5-nel nagyobb szamok szurese... \n");
    while(fgets(line, MAX, fp) != NULL)
    {
        line[strlen(line) - 1] = '\0';
        float number = atof(line);

        if(number > 0.5 && wrote != 0)
        {
            fprintf(out, "\n");
            fprintf(out, "%.20f", number);
            wrote++;
        }
        else if(number > 0.5)
        {
            fprintf(out, "%.20f", number);
            wrote++;
        }

    }
    printf("# szures vege \n");

    fclose(fp);
    fclose(out);
    printf("# out.txt bezarva \n");

    printf("# out.txt-be kiirt szamok mennyisege: %d db\n", wrote);

    return 0;
}