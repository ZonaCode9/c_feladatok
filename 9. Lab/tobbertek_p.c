#include <stdio.h>
#include <stdlib.h>

int randint(int also, int felso)
{
    int veletlen = rand();
    int intervallum = felso - also + 1;

    veletlen = veletlen % intervallum;
    veletlen = veletlen + also;

    return veletlen;
}

void feltolt(int tomb[])
{
    printf("A tomb elemei: ");
    for(int i = 0; i < 10; i++)
    {
        tomb[i] = randint(10, 99);
        printf("%d, ", tomb[i]);
    }
    puts("");
}

int kisebbnagyobb(int *legkisebb, int *legnagyobb, float *atlag, const int tomb[])
{
    *legkisebb = tomb[0];
    *legnagyobb = tomb[0];

    float sum = tomb[0];

    for(int i = 1; i < 10; i++)
    {
        if(*legkisebb > tomb[i])
        {
            *legkisebb = tomb[i];
        }

        if(*legnagyobb < tomb[i])
        {
            *legnagyobb = tomb[i];
        }

        sum += tomb[i];
    }

    *atlag = sum / 10;
}

int main()
{
    int smallest = 0;
    int biggest = 0;
    float avg = 0;

    srand(69);
    int szamok[10] = { 0 };

    feltolt(szamok);

    kisebbnagyobb(&smallest, &biggest, &avg, szamok);

    printf("A legkisebb elem: %d \n", smallest);
    printf("A legnagyobb elem: %d \n", biggest);
    printf("Az elemek atlaga: %.1f \n", avg);

    return 0;
}