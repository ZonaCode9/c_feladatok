#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 1000

int main(int argc, char* argv[])
{

    if(argc != 2)
    {
        fprintf(stderr, "Hiba! Adja meg egy szoveges allomany nevet! \n");
        exit(1);
    }

    char* fname = argv[1];

    FILE *fp = fopen(fname, "r");

    if(fp == NULL)
    {
        fprintf(stderr, "Hiba! A %s nevu fajlt nem sikerult megnyitni! \n", fname);
        exit(1);
    }

    char line[MAX];
    int lines = 0;

    while(fgets(line, MAX, fp) != NULL)
    {
        lines++;
    }

    fclose(fp);

    printf("%d \n", lines);

    return 0;
}