#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "prog1.h"

int is_valid_c_identifier(const char* input)
{
    int length = strlen(input);

    if(length < 1)
    {
        return 0;
    }

    if(input[0] != '_' && !isalpha(input[0]))
    {
        return 0;
    }

    for(int i = 1; i < length; i++)
    {
        if( input[i] != '_' && !isalpha(input[i]) && !isdigit(input[i]) )
        {
            return 0;
        }
    }

    return 1;
}

int main()
{
    char* word;

    puts("Adj meg sztringeket a '*' vegjelig!");
    puts("");

    while(1)
    {
        word = get_string("Input: ");

        if(strcmp(word, "*") == 0)
        {
            break;
        }

        if( is_valid_c_identifier(word) )
        {
            puts("YES");
        }
        else if( !is_valid_c_identifier(word) )
        {
            puts("NO");
        }

        puts("");

    }

    return 0;
}