#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 1000

float atalakit(char* szam)
{
    int hossz = strlen(szam);

    for(int i = 0; i < hossz; i++)
    {
        if(szam[i] == ',')
        {
            szam[i] = '.';
            break;
        }
    }

    return atof(szam);
}

int main()
{
    char* fname = "valos_szamok.txt";

    FILE *fp = fopen(fname, "r");

    if(fp == NULL)
    {
        fprintf(stderr, "Hiba! A %s nevu fajlt nem sikerult megnyitni! \n", fname);
        exit(1);
    }

    char line[MAX];
    float sum = 0;

    while(fgets(line, MAX, fp) != NULL)
    {
        line[strlen(line) - 1] = '\0';
        sum += atalakit(line);
    }

    fclose(fp);

    printf("%.20f \n", sum);

    return 0;
}