#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void legrovidebbek(const int length, char* words[])
{
    int minhossz = strlen(words[1]);
    for(int i = 2; i < length; i++)
    {
        if(strlen(words[i]) < minhossz)
        {
            minhossz = strlen(words[i]);
        }
    }

    for(int i = 1; i < length; i++)
    {
        if(strlen(words[i]) == minhossz)
        {
            puts(words[i]);
        }
    }
}

int main(int argc, char* argv[])
{
    if(argc == 1)
    {
        fprintf(stderr, "Nem adtal meg egyetlen szot sem! \n");
        exit(1);
    }

    legrovidebbek(argc, argv);

    return 0;
}