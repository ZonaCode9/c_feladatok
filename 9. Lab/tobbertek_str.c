#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int smallest;
    int biggest;
    float avg;
} Special;

int randint(int also, int felso)
{
    int veletlen = rand();
    int intervallum = felso - also + 1;

    veletlen = veletlen % intervallum;
    veletlen = veletlen + also;

    return veletlen;
}

void feltolt(int tomb[])
{
    printf("A tomb elemei: ");
    for(int i = 0; i < 10; i++)
    {
        tomb[i] = randint(10, 99);
        printf("%d, ", tomb[i]);
    }
    puts("");
}

Special kisebbnagyobb(Special keresett, const int tomb[])
{
    keresett.smallest = tomb[0];
    keresett.biggest = tomb[0];
    keresett.avg = 0;

    float sum = tomb[0];

    for(int i = 1; i < 10; i++)
    {
        if(keresett.smallest > tomb[i])
        {
            keresett.smallest = tomb[i];
        }

        if(keresett.biggest < tomb[i])
        {
            keresett.biggest = tomb[i];
        }

        sum += tomb[i];
    }

    keresett.avg = sum / 10;

    return keresett;
}

int main()
{
    Special special_values;

    srand(69);
    int szamok[10] = { 0 };

    feltolt(szamok);

    special_values = kisebbnagyobb(special_values, szamok);

    printf("A legkisebb elem: %d \n", special_values.smallest);
    printf("A legnagyobb elem: %d \n", special_values.biggest);
    printf("Az elemek atlaga: %.1f \n", special_values.avg);

    return 0;
}