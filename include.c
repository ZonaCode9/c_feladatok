#include <stdio.h>

int include(int nagysag, int keresett, int tomb[])
{
    int includes = 0;
    for(int i = 0; i < nagysag; i++)
    {
        if(tomb[i] == keresett)
        {
            includes = 1;
            break;
        }
    }
    return includes;
}

int main()
{
    int keresett;
    puts("Adja meg a keresett szmot!");
    scanf("%d", &keresett);

    int szamok[] = { 1, 2, 3, 5, 6, 7 };
    int tomb_nagysaga = sizeof(szamok) / sizeof(szamok[0]);

    int tartalmaz = include(tomb_nagysaga, keresett, szamok);

    printf(tartalmaz == 1 ? "Tartalmazza." : "Nem tartalmazza.");

    return 0;
}
