#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 1000

void shellsort(int n, int array[])
{
    for(int interval = n / 2; interval > 0; interval /= 2)
    {
        for(int i = interval; i < n; i++)
        {
            int temp = array[i];
            int j;
            for(j = i; j >= interval && array[j - interval] > temp; j -= interval)
            {
                array[j] = array[j - interval];
            }
            array[j] = temp;
        }
    }
}

void kiir(int n, int tomb[])
{
    for(int i = 0; i < n; i++)
    {
        printf("%d \n", tomb[i]);
    }
}

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        fprintf(stderr, "Hiba! Adja meg egy szoveges allomany nevet! \n");
        exit(1);
    }

    char* fname = argv[1];

    FILE *fp = fopen(fname, "r");

    if(fp == NULL)
    {
        fprintf(stderr, "Hiba! A %s nevu fajlt nem sikerult megnyitni! \n", fname);
        exit(1);
    }

    char line[MAX];
    int lines = 0;

    while(fgets(line, MAX, fp) != NULL)
    {
        lines++;
    }
    fclose(fp);


    int *tomb = malloc(lines * sizeof(int));
    FILE *myfile = fopen(fname, "r");

    char sor[MAX];
    int szamol = 0;
    while(fgets(sor, MAX, myfile) != NULL)
    {
        sor[strlen(sor) - 1] = '\0';
        tomb[szamol] = atoi(sor);
        szamol++;
    }

    shellsort(szamol, tomb);

    kiir(szamol, tomb);


    fclose(myfile);
    free(tomb);

    return 0;
}