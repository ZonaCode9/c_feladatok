#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int randint(const int also, const int felso)
{
    int random = rand();
    int interval = felso - also + 1;

    random = random % interval;
    random = also + random;

    return random;
}

int choice(const int n, const int tomb[])
{
    int index = randint(0, n - 1);

    return tomb[index];
}

int main()
{
    srand(time(NULL));

    int szamok[] = {10, 3, 5, 5674, 467};
    int db = sizeof(szamok) / sizeof(szamok[0]);

    printf("%d \n", choice(db, szamok));

    return 0;
}