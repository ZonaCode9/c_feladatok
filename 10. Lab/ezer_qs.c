#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 1000

void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

int part(int low, int high, int tomb[])
{
    int pivot = tomb[high];

    int i = (low - 1);

    for(int j = low; j < high; j++)
    {
        if(tomb[j] <= pivot)
        {
            i++;
            swap(&tomb[i], &tomb[j]);
        }
    }

    swap(&tomb[i + 1], &tomb[high]);

    return (i + 1);
}

void quicksort(int low, int high, int tomb[])
{
    if(low < high)
    {
        int pi = part(low, high, tomb);
        quicksort(low, pi - 1, tomb);
        quicksort(pi + 1, high, tomb);
    }
}

void kiir(int n, int tomb[])
{
    for(int i = 0; i < n; i++)
    {
        printf("%d \n", tomb[i]);
    }

}

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        fprintf(stderr, "Hiba! Adja meg egy szoveges allomany nevet! \n");
        exit(1);
    }

    char* fname = argv[1];

    FILE *fp = fopen(fname, "r");

    if(fp == NULL)
    {
        fprintf(stderr, "Hiba! A %s nevu fajlt nem sikerult megnyitni! \n", fname);
        exit(1);
    }

    char line[MAX];
    int lines = 0;

    while(fgets(line, MAX, fp) != NULL)
    {
        lines++;
    }
    fclose(fp);


    int *tomb = malloc(lines * sizeof(int));
    FILE *myfile = fopen(fname, "r");

    char sor[MAX];
    int szamol = 0;
    while(fgets(sor, MAX, myfile) != NULL)
    {
        sor[strlen(sor) - 1] = '\0';
        tomb[szamol] = atoi(sor);
        szamol++;
    }

    quicksort(0, szamol, tomb);

    kiir(szamol, tomb);


    fclose(myfile);
    free(tomb);

    return 0;
}