#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int randint(const int also, const int felso)
{
    int random = rand();
    int interval = felso - also + 1;

    random = random % interval;
    random = also + random;

    return random;
}

void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void shuffle(int n, int tomb[])
{
    for(int i = n - 1; i > 0; i--)
    {
        int j = randint(0, i);
        swap(&tomb[i], &tomb[j]);
    }
}

void kiir(const int n, const int tomb[])
{
    for(int i = 0; i < n; i++)
    {
        printf("%d ", tomb[i]);
    }
    puts("");
}

int main()
{
    srand(time(NULL));

    int szamok[] = {10, 3, 5, 5674, 467};
    int db = sizeof(szamok) / sizeof(szamok[0]);

    kiir(db, szamok);

    shuffle(db, szamok);

    kiir(db, szamok);

    return 0;
}