#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <string.h>

int randint(const int also, const int felso)
{
    int random = rand();
    int interval = felso - also + 1;

    random = random % interval;
    random = also + random;

    return random;
}

int is_strong (char* password)
{
    int has_num = 0;
    int has_lower = 0;
    int has_upper = 0;
    int has_spec = 0;

    int length = strlen(password);
    int long_enough = (length >= 8);

    if(long_enough)
    {
        for(int i = 0; i < length; i++)
        {
            if(isdigit(password[i]))
            {
                has_num = 1;
            }
            else if(islower(password[i]))
            {
                has_lower = 1;
            }
            else if(isupper(password[i]))
            {
                has_upper = 1;
            }
            else if(password[i] == '.' || password[i] == ',' || password[i] == ';' || password[i] == '\'')
            {
                has_spec = 1;
            }
        }
    }

    return long_enough && has_upper && has_lower && has_num && has_spec;
}

char generate()
{
    char* lower= "abcdefghijklmnopqrstuvwxyz";
    char* upper= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char* nums= "1234567890";
    char* spec= ".,;'";

    int choice = randint(1, 4);
    char result;

    if(choice == 1)
    {
        result = lower[randint(0, 25)];
    }
    else if(choice == 2)
    {
        result = upper[randint(0, 25)];
    }
    else if(choice == 3)
    {
        result = nums[randint(0, 9)];
    }
    else
    {
        result = spec[randint(0, 3)];
    }

    return result;
}

void passgen()
{
    int hossz = randint(8, 12);

    char passw[hossz + 1];

    do
    {
        for(int i = 0; i < hossz; i++)
        {
            passw[i] = generate();
        }
        passw[hossz] = '\0';
    } while (!is_strong(passw));

    puts(passw);
}

int main()
{
    srand(time(NULL));

    passgen();

    return 0;
}