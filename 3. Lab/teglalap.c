#include <stdio.h>

int kerulet (int a, int b)
{
    return 2 * (a + b);
}

int terulet (int a, int b)
{
    return a * b;
}

int main()
{
    int szelesseg = 0;
    int magassag = 0;

    printf("Adja meg egy teglalap szelesseget egesz szammal: ");
    scanf("%d", &szelesseg);
    printf("Adja meg egy teglalap magassagat egesz szammal: ");
    scanf("%d", &magassag);

    printf("A %d szeles, %d magas teglalap kerulete: %d, terulete: %d \n", szelesseg, magassag, kerulet(szelesseg, magassag), terulet(szelesseg, magassag));
    return 0;
}