#include <stdio.h>

int main()
{
    int magassag = 0;

    printf("magassag: ");
    scanf("%d", &magassag);

    for(int i = 1; i <= magassag; i++)
    {
        //A fal elso fele
        int whitespaces = magassag - i;
        for(int j = 1; j <= whitespaces; j++)
        {
            printf(" ");
        }

        for(int j = 1; j <= i; j++)
        {
            printf("#");
        }

        //a fal masodik fele
        printf("  ");
        for(int j = 1; j <= i; j++)
        {
            printf("#");
        }

        printf("\n");
    }

    return 0;
}
