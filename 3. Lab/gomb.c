#include <stdio.h>
#include <math.h>

double get_double ()
{
    double megadott;
    printf("Adja meg egy gomb sugarat valos szammal: ");
    scanf("%lf", &megadott);

    return megadott;
}

float felszin (float r)
{
    return 4 * M_PI * r * r;
}

float terfogat (float r)
{
    return (4 * M_PI * r * r * r) / 3;
}

int main()
{
    double sugar = get_double();

    printf("A %lf sugaru gomb felszine: %lf, terfogata: %lf. \n", sugar, felszin(sugar), terfogat(sugar));

    return 0;
}