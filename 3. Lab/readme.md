# FELADATOK

## Mario v3 - 20200220g

Írjunk egy programot, ami bekér egy magasságot, majd kirajzol két derékszögű háromszöget egymás mellé az alább látható módon. (A két háromszög között 2 db szóköz legyen).

## Diamond - 20200213j

Írjunk egy programot, ami interaktív módon bekéri egy gyémánt magasságát (egész szám).
Először ellenőrizzük le, hogy helyes-e a bemenet. Csak pozitív páratlan számot fogadjunk el, ellenkező esetben írjunk ki egy hibaüzenetet és a program álljon le.
Helyes bemenet (pozitív páratlan szám) esetén rajzoljuk ki a gyémántot.

## Téglalap - függvénnyel

Írjunk egy programot, ami bekéri egy téglalap oldalainak a hosszát, majd írassuk ki a téglalap kerületét, területét. A kerület és terület kiszámítását függvénnyel oldjuk meg!

## Kör - függvénnyel

Írjunk egy programot, ami bekéri egy kör sugarát (egész érték), majd írassuk ki a kör kerületét, területét. A kerület és terület kiszámítását függvénnyel oldjuk meg!

## Gömb - függvénnyel

Írjunk egy programot, ami bekéri egy gömb sugarát (valós érték), majd írassuk ki a gömb felszínét és térfogatát. Három függvényre is szükség lesz: get_double(), amivel egy valós értéket olvasunk be a billentyűzetről, ill. a felszín és térfogat kiszámítását is függvénnyel oldjuk meg!

## Get_positive_int

Egy pozitív számot kér be, ha hibás a bemenet, akkor addig próbálja újra, ameddig helyes bemenetet nem kap.
