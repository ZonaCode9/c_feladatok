#include <stdio.h>

int main()
{
    int magassag = 0;

    printf("Adjon meg egy pozitiv, paratlan szamot! ");
    scanf("%d", &magassag);

    if(magassag % 2 != 0 && magassag > 0)
    {
        int noves = (magassag / 2) + 1;
        for(int i = 1; i <= noves; i++)
        {
            int whitespaces = noves - i;

            for(int j = 1; j <= whitespaces; j++)
            {
                printf(" ");
            }

            int stars = magassag - (2 * whitespaces);
            for(int j = 1; j <= stars; j ++)
            {
                printf("*");
            }
            printf("\n");
        }

        int csokkenes = magassag - noves;
        for(int i = 1; i <= csokkenes; i++)
        {
            int whitespaces = i;

            for(int j = 1; j <= whitespaces; j++)
            {
                printf(" ");
            }

            int stars = magassag - (2 * whitespaces);
            for(int j = 1; j <= stars; j ++)
            {
                printf("*");
            }
            printf("\n");
        }
    }
    else
    {
        printf("Hiba. Ellenorizze a megadott szamot!");
    }

    return 0;
}
