#include <stdio.h>

int get_positive_int ()
{
    int szam = 0;
    while(1)
    {
        printf("Adjon meg egy pozitiv szamot: ");
        scanf("%d", &szam);

        if(szam > 0)
        {
            break;
        }
        else
        {
            printf("Hiba. Pozitiv szamot adjon meg! \n");
        }
    }
    return szam;
}

int main()
{
    int bekert_szam = get_positive_int();

    printf("A megadott szam: %d \n", bekert_szam);

    return 0;
}
