#include <stdio.h>
#include <math.h>

double kerulet (int r)
{
    return 2 * r * M_PI;
}

double terulet (int r)
{
    return (r * r) * M_PI;
}

int main()
{
    int sugar = 0;

    printf("Adja meg egy kor sugarat egesz szammal: ");
    scanf("%d", &sugar);

    printf("A %d sugaru kor kerulete: %lf, terulete: %lf \n", sugar, kerulet(sugar), terulet(sugar));

    return 0;
}