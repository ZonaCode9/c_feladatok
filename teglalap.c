#include <stdio.h>

int main() {

    //bekerendo adatok
    int hosszusag = 0;
    int magassag = 0;

    //adatbekeres
    printf("Adja meg a teglalap hosszusagat egesz cm-ben! \n");
    scanf("%d", &hosszusag);
    printf("Adja meg a teglalap magassagat egesz cm-ben! \n");
    scanf("%d", &magassag);

    //kerulet es terulet szamitasa
    int kerulet = 2 * (hosszusag + magassag);
    int terulet = hosszusag * magassag;

    //az eredmenyek kiirasa
    printf("A %d cm hosszu es %d cm magas teglalap kerulete: %d cm, \n", hosszusag, magassag, kerulet);
    printf("Terulete: %d cm \n", terulet);

    return 0;
}