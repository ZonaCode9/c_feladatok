#include <stdio.h>

void tombkiir(int hossz, int tomb[])
{
    for(int i = 0; i < hossz - 1; i++)
    {
        printf("%d, ", tomb[i]);
    }
    printf("%d\n", tomb[hossz-1]);
}

int main()
{
    int elemek[] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };

    int hosszusag = sizeof(elemek) / sizeof(elemek[0]);

    tombkiir(hosszusag, elemek);

    return 0;
}
