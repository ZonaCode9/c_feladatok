#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX 1000

typedef char* string;

int randint(const int also, const int felso)
{
    int random = rand();
    int interval = felso - also + 1;

    random = random % interval;
    random = also + random;

    return random;
}

void swap(char *a, char *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void my_strfry(string string)
{
    int length = strlen(string);

    for(int i = length - 1; i > 0; i--)
    {
        int j = randint(0, i);
        swap(&string[i], &string[j]);
    }
}

int main()
{
    srand(time(NULL));

    char szo[MAX];

    printf("Adj meg egy sztringet: ");

    fgets(szo, MAX, stdin);
    szo[strlen(szo) - 1] = '\0';

    puts(szo);

    my_strfry(szo);

    puts(szo);

    return 0;
}