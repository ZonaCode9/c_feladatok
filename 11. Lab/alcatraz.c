#include <stdio.h>
#include <stdlib.h>

#define ARR_SIZE 601

int invert(int x)
{
    return 1 - x;
}

void opener(int tomb[])
{
    for(int i = 1; i < ARR_SIZE; i++) //i az adott szam, ahanyadik elemeket valtoztatjuk
    {
        for(int j = i; j < ARR_SIZE; j++) //j egy adott indexu elem
        {
            if(j % i == 0)
            {
                tomb[j] = invert(tomb[j]);
            }
        }
    }
}

void got_away(int tomb[])
{
    for(int i = 1; i < ARR_SIZE; i++)
    {
        if(tomb[i] == 1)
        {
            printf("%d", i);
        }
    }
    puts("");
}

int main()
{
    int *inmates = malloc(ARR_SIZE * sizeof(int));

    for(int i = 0; i < ARR_SIZE; i++)
    {
        inmates[i] = 0;
    }

    opener(inmates);

    got_away(inmates);

    free(inmates);

    return 0;
}