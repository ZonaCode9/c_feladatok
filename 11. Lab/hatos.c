#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SUM 90
#define PRODUCT 996300

int is_present(const int number, const int hossz, int szamok[])
{
    for(int i = 0; i < hossz; i++)
    {
        if(number == szamok[i])
        {
            return 1;
        }
    }
    return 0;
}

void feltolt(const int n, int tomb[])
{
    for(int i = 0; i < n; i++)
    {
        int szam = (rand() % 45) + 1;

        while(is_present(szam, n, tomb))
        {
            szam = (rand() % 45) + 1;
        }
        tomb[i] = szam;
    }
}

void sort(const int darab, int szamok[])
{

    for(int i = 0; i < darab - 1; i++)
    {
        for(int j = i; j < darab; j++)
        {
            if(szamok[i] > szamok[j])
            {
                int tmp = 0;
                tmp = szamok[i];
                szamok[i] = szamok[j];
                szamok[j] = tmp;
            }
        }
    }
}

void kiir(const int n, const int szamok[])
{
    for(int i = 0; i < n; i++)
    {
        printf("%d ", szamok[i]);
    }
    puts(" ");
}

int sum(const int n, const int szamok[])
{
    int sum = 0;

    for(int i = 0; i < n; i++)
    {
        sum += szamok[i];
    }
    return sum;
}

int product(const int n, const int szamok[])
{
    int prod = 0;

    for(int i = 0; i < n; i++)
    {
        prod *= szamok[i];
    }
    return prod;
}

void guesser(const int n, int szamok[])
{
    do {
        feltolt(n, szamok);
    } while( sum(n, szamok) != SUM || product(n, szamok) != PRODUCT );
}

int main()
{
    srand(time(NULL));

    int szamok[6];

    guesser(6, szamok);

    sort(6, szamok);

    kiir(6, szamok);

    return 0;
}