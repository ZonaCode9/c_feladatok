#include <stdio.h>
#include <math.h>

void prime_factors(int n)
{
    int sum = 0;

    while(n % 2 == 0)
    {
        printf("%d ", 2);
        sum += 2;
        n = n / 2;
    }

    for(int i = 3; i <= sqrt(n); i += 2)
    {
        while(n % i == 0)
        {
            printf("%d ", i);
            sum += i;
            n = n / i;
        }
    }

    if(n > 2)
    {
        printf("%d ", n);
        sum += n;
    }

    printf("%d ", sum);
}

int main()
{
    prime_factors(996300);

    return 0;
}