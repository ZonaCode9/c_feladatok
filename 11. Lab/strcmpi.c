#include <stdio.h>
#include <string.h>

int abs(const int a)
{
    return a > 0 ? a : -a;
}

int strcmpi(const char* s1, const char* s2)
{
    int length_diff = strlen(s1) - strlen(s2);

    if(length_diff > 0)
    {
        return 1;
    }
    else if(length_diff < 0)
    {
        return -1;
    }

    int length = strlen(s1);

    int i;

    for( i = 0; i < length; i++)
    {
        if( s1[i] == s2[i] || (abs(s1[i] - s2[i]) == 32) )
        {
            continue;
        }
        else
        {
            return s1[i] - s2[i];
        }
    }

    return 0;
}

int main()
{
    printf("%d \n", strcmpi("pti", "PTi"));

    return 0;
}