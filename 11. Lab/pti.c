#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 200

typedef char* string;

int abs(const int a)
{
    return a > 0 ? a : -a;
}

int strcmpi(const char* s1, const char* s2)
{
    int length_diff = strlen(s1) - strlen(s2);

    if(length_diff > 0)
    {
        return 1;
    }
    else if(length_diff < 0)
    {
        return -1;
    }

    int length = strlen(s1);

    int i;

    for( i = 0; i < length; i++)
    {
        if( s1[i] == s2[i] || (abs(s1[i] - s2[i]) == 32) )
        {
            continue;
        }
        else
        {
            return s1[i] - s2[i];
        }
    }

    return 0;
}

void sort(const int szamol, string nev[])
{
    char temp[100];

    for(int i=0; i<szamol; i++){
    for(int j=0; j<szamol-1-i; j++){
      if(strcmp(nev[j], nev[j+1]) > 0){
        //swap 
        strcpy(temp, nev[j]);
        strcpy(nev[j], nev[j+1]);
        strcpy(nev[j+1], temp);
      }
    }
  }
}

void kiir(const int szamol, const string nev[])
{
    for(int i = 0; i < szamol; i++)
    {
        puts(nev[i]);
    }
}

int main()
{
    FILE *fp = fopen("nevek.csv", "r");
    if( fp == NULL)
    {
        fprintf(stderr, "Hiba!\n");
        exit(1);
    }

    char sor[BUFSIZE];
    char *p;
    string nev;
    int kor;
    string szak;

    string nevek[100];
    int szamol = 0;

    while(fgets(sor, BUFSIZE, fp) != NULL)
    {
        sor[strlen(sor) - 1] = '\0';

        p = strtok(sor, ",");
        nev = p;
        p = strtok(NULL, ",");
        kor = atoi(p);
        p = strtok(NULL, ",");
        szak = p;

        if(strcmpi(szak, "PTI") == 0)
        {
            nevek[szamol] = nev;
            printf("%s \n", nevek[szamol]);
            szamol++;
        }
    }

    for(int i = 0; i < szamol; i++)
    {
        printf("%s \n", nevek[szamol]);
    }

    fclose(fp);

    return 0;
}