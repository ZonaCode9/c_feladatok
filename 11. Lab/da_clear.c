#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 2000
#define INITIAL_CAPACITY 20
#define MULTIPLIER 1.5

typedef struct {
    int *elems;
    int length;
    int capacity;
} DynArray;

void mem_error_exit()
{
    fprintf(stderr, "Error: Cannot allocate memory!\n");
    exit(1);
}

void file_error_exit(char *fname)
{
    fprintf(stderr, "Error: '%s' cannot be opened!\n", fname);
    exit(1);
}

DynArray * da_create()
{
    DynArray *result = malloc(sizeof(DynArray));

    if(result == NULL) {
        mem_error_exit();
    }

    result->elems = malloc(INITIAL_CAPACITY * sizeof(int));
    if(result->elems == NULL) {
        mem_error_exit();
    }
    result->length = 0;
    result->capacity = INITIAL_CAPACITY;

    return result;
}

void da_append(DynArray *self, int data)
{
    if(self->length == self->capacity)
    {
        int new_capacity = (int)(MULTIPLIER * self->capacity);
        self->elems = realloc(self->elems, new_capacity * sizeof(int));
        if(self->elems == NULL) {
            mem_error_exit();
        }
        self->capacity = new_capacity;
    }

    self->elems[self->length] = data;
    self->length += 1;
}

void * da_destroy(DynArray *self)
{
    free(self->elems);
    free(self);

    return NULL;
}

void da_clear(DynArray *self)
{
    for(int i = 0; i < self->length; i++)
    {
        self->elems[i] = 0;
    }
}

void kiir(DynArray *self)
{
    for(int i = 0; i < self->length; i++)
    {
        printf("%d ", self->elems[i]);
    }
    puts("");
}

int main()
{
    DynArray *li = da_create();

    for(int i = 0; i < 101; i++)
    {
        da_append(li, i);
    }

    kiir(li);

    da_clear(li);

    kiir(li);

    li = da_destroy(li);

    return 0;
}