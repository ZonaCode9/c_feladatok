#include <stdio.h>

int elemszorzat(int hossz, int tomb[])
{
    int szorzat = 1;
    for(int i = 0; i < hossz; i++)
    {
        szorzat *= tomb[i];
    }
    return szorzat;
}

int main()
{
    int tomb[] = { 1, 2, 4, 5};
    int tombnagysag = sizeof(tomb) / sizeof(tomb[0]);

    printf("A tomb elemeinek szorzata: %d \n", elemszorzat(tombnagysag, tomb));

    return 0;
}
