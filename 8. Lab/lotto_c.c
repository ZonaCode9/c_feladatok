#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int randint(int also, int felso)
{
    int veletlen = rand();
    int intervallum = felso - also + 1;

    veletlen = veletlen % intervallum;
    veletlen = also + veletlen;

    return veletlen;
}

int is_present(const int number, const int hossz, int szamok[])
{
    for(int i = 0; i < hossz; i++)
    {
        if(number == szamok[i])
        {
            return 1;
        }
    }
    return 0;
}

void feltolt(const int darab, const int also, const int felso, int tomb[])
{
    srand(time(NULL));

    for(int i = 0; i < darab; i++)
    {
        int szam = randint(also, felso);

        while(is_present(szam, darab, tomb))
        {
            szam = randint(also, felso);
        }
        tomb[i] = szam;
    }
}

void sort(const int darab, int szamok[])
{

    for(int i = 0; i < darab - 1; i++)
    {
        for(int j = i; j < darab; j++)
        {
            if(szamok[i] > szamok[j])
            {
                int tmp = 0;
                tmp = szamok[i];
                szamok[i] = szamok[j];
                szamok[j] = tmp;
            }
        }
    }
}

int main()
{
    int also_hatar = 0;
    int felso_hatar = 0;
    int darab = 0;

    puts("Hany darab szamot kersz? ");
    scanf("%d", &darab);

    printf("Also hatar: ");
    scanf("%d", &also_hatar);

    printf("Felso hatar (zart intervallum): ");
    scanf("%d", &felso_hatar);

    int szamok[darab];

    feltolt(darab, also_hatar, felso_hatar, szamok);

    sort(darab, szamok);

    for(int i = 0; i < darab; i++)
    {
        printf("%d ", szamok[i]);
    }
    puts(" ");

    return 0;
}