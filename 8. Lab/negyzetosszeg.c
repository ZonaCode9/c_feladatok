#include <stdio.h>

int squared(const int szam)
{
    return szam * szam;
}

int sum_of_square(const int limit)
{
    int sum = 0;

    for(int i = 1; i <= limit; i++)
    {
        sum += squared(i);
    }

    return sum;
}

int square_of_sum(const int limit)
{
    int sum = 0;

    for(int i = 1; i <= limit; i++)
    {
        sum += i;
    }

    return squared(sum);
}

int main()
{
    printf("%d \n", square_of_sum(100) - sum_of_square(100));

    return 0;
}