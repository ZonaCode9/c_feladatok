#include <stdio.h>
#include <string.h>
#include "prog1.h"

int szamjegyek_osszege(string s)
{
    int hossz = strlen(s);
    int sum = 0;

    for(int i = 0; i < hossz; i++)
    {
        sum += s[i] - '0';
    }

    return sum;
}

int main()
{
    string szoveg = get_string("Szam: ");

    printf("A szamjegyek osszege: %d \n", szamjegyek_osszege(szoveg));

    return 0;
}