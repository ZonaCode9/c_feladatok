#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void tombkiir(const int n, const int tomb[])
{
    for (int i = 0; i < n; i++)
    {
        if (i != n-1)
        {
            printf("%d, ", tomb[i]);
        }
        else
        {
            printf("%d\n", tomb[i]);
        }
    }

}

int not_present(const int n, const int tomb[], int k)
{
    for (int i = 0; i < n; i++)
    {
        if (k == tomb[i])
        {
            return 0;
        }

    }

    return 1;
}

void rendez(const int n, int tomb[])
{
    int temp = 0;
    for (int i = 0; i < n-1; i++)
    {
        for (int j = i; j < n; j++)
        {
            if(tomb[i] > tomb[j])
            {
                temp = tomb[i];
                tomb[i] = tomb[j];
                tomb[j] = temp;
            }
        }
    }
}

int main()
{
    int tomb[99] = {0};

    int bekert = 0;
    int darab = 0;
    printf("Adj meg 0 v�gjelig eg�sz sz�mokat az [1, 99] intervallumb�l!\n");
    printf("Sz�m: ");
    scanf("%d", &bekert);
    while(bekert != 0)
    {
        if(bekert < 1 || bekert > 99)
        {
            printf("Ez a sz�m k�v�l esik az elfogadhat� intervallumon!\n");
        }
        else
        {
            if (not_present(99, tomb, bekert))
            {
                tomb[darab] = bekert;
                darab++;
            }

        }
        printf("Sz�m: ");
        scanf("%d", &bekert);
    }

    rendez(darab, tomb);

    printf("%d db k�l�nb�z� sz�m lett megadva. \n", darab);

    printf("Ezek (n�vekv� sorrendben): ");
    tombkiir(darab, tomb);

    return 0;
}
