#include <stdio.h>

int abs(int n)
{
    return n >= 0 ? n : -n;
}

void feltolt(const int n, int tomb[])
{
    for(int i = 0; i < n; i++)
    {
        printf("%d. szám: ", i + 1);
        scanf("%d", &tomb[i]);
    }
}

void pozitiv(const int n, int elotte[], int utana[])
{
    for(int i = 0; i < n; i++)
    {
        utana[i] = abs(elotte[i]);
    }
}

int main()
{
    int szamdarab = 0;

    puts("Hány számot szeretnél bevinni?");
    scanf("%d", &szamdarab);

    int megadott_szamok[szamdarab];

    feltolt(szamdarab, megadott_szamok);

    int abszolut_szamok[szamdarab];

    pozitiv(szamdarab, megadott_szamok, abszolut_szamok);

    printf("A bevitt szamok abszolutertekei: ");
    for(int i = 0; i < szamdarab - 1; i++)
    {
        printf("%d, ", abszolut_szamok[i]);
    }
    printf("%d \n", abszolut_szamok[szamdarab - 1]);

    printf("A megadott szamok: ");
    for(int i = 0; i < szamdarab - 1; i++)
    {
        printf("%d, ", megadott_szamok[i]);
    }
    printf("%d \n", megadott_szamok[szamdarab - 1]);

    return 0;
}