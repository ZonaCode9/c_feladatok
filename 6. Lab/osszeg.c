#include <stdio.h>
#include "prog1.h"
#include <stdlib.h>

int main(int argc, string argv[])
{
    if(argc == 3)
    {
        int osszeg = atoi(argv[1]) + atoi(argv[2]);
        printf("%d \n", osszeg);
    }
    else
    {
        puts("Hiba! Ket parametert (szamot) kell megadni.");
    }

    return 0;
}