#include <stdio.h>
#include <stdlib.h>

int main()
{
    int szam_db = 0;
    int also = 0;
    int felso = 0;
    puts("Hany db szamot kersz?");
    scanf("%d", &szam_db);

    int szamok[szam_db];

    printf("Also hatar: ");
    scanf("%d", &also);

    printf("Felso hatar (zart intervallum): ");
    scanf("%d", &felso);

    for(int i = 0; i < szam_db; i++)
    {
        int szam = (rand() % (felso - also)) + also;
        szamok[i] = szam;
    }

    printf("A generalt szamok: ");
    for(int i = 0; i < szam_db; i++)
    {
        printf("%d ", szamok[i]);
    }
    puts("");

    return 0;
}