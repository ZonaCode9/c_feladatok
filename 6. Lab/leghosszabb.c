#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int main()
{
    int darab = 0;
    long maximum = 0;
    char szo[1000];
    printf("Adj meg szavakat '*' végjelig! \n");

    do {
        printf("Szó: ");
        fgets(szo, 1000, stdin);
        if(strlen(szo)-1 > maximum)
        {
            maximum = strlen(szo)-1;
        }
        darab++;
    } while(szo[0] != '*' || strlen(szo)-1 != 1);

    puts("");

    printf("%d db szot adott meg. A leghosszabb szo %ld karakterből áll. \n", darab-1, maximum);

    return 0;
}