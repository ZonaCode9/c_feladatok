#include <stdio.h>
#define SIZE 26

void feltolt(char tomb[])
{
    for(int i = 0; i < SIZE; i++)
    {
        tomb[i] = i + 'a';
    }
    tomb[SIZE] = '\0';
}

int main()
{
    char abc[SIZE + 1];

    feltolt(abc);

    printf("%ld \n", sizeof(abc));

    for(int i = 0; i < SIZE + 1; i++)
    {
        putchar(abc[i]);
        putchar(' ');
    }

    return 0;
}